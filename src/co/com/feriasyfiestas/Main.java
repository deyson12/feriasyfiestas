package co.com.feriasyfiestas;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import co.com.feriasyfiestas.dto.FeriaFiesta;

/**
 * Clase Inicial
 * 
 * @author 
 */
public class Main {
	
	// Se crean los escaneres necesarios para leer los String y los Enteros
	Scanner escanerString = new Scanner(System.in);
	Scanner escanerEntero = new Scanner(System.in);
	
	// Se crea la lista para almacenar las ferias y fiestas
	Map<String, FeriaFiesta> listaFeriasyFiestas = new HashMap<>();

	/**
	 * Metodo inicial para correr el programa
	 * @param args
	 */
	public static void main(String[] args) {
		// Iniciamos el programa
		Main main = new Main();
		main.iniciar();
	}

	/**
	 * Metodo SOLO para pruebas
	 */
	private void llenarPruebas() {
		
		// LLenamos las Ferias
		FeriaFiesta feria1 = new FeriaFiesta();
		feria1.setTipo("Feria");
		feria1.setCodigo("001");
		feria1.setNombre("San Pedro");
		feria1.setFechaInicio("01-01-2020");
		feria1.setCantidadDias(3);
		feria1.setMunicipio("Envigado");
		feria1.setAntiguedad(3);
		
		listaFeriasyFiestas.put(feria1.getCodigo(), feria1);
		
		FeriaFiesta feria2 = new FeriaFiesta();
		feria2.setTipo("Feria");
		feria2.setCodigo("002");
		feria2.setNombre("San Nicolas");
		feria2.setFechaInicio("01-02-2020");
		feria2.setCantidadDias(2);
		feria2.setMunicipio("Envigado");
		feria2.setAntiguedad(2);
		
		listaFeriasyFiestas.put(feria2.getCodigo(), feria2);
		
		FeriaFiesta feria3 = new FeriaFiesta();
		feria3.setTipo("Feria");
		feria3.setCodigo("003");
		feria3.setNombre("San Carlos");
		feria3.setFechaInicio("01-03-2020");
		feria3.setCantidadDias(5);
		feria3.setMunicipio("Medell�n");
		feria3.setAntiguedad(5);
		
		listaFeriasyFiestas.put(feria3.getCodigo(), feria3);
		
		// Llenamos las Fiestas
		FeriaFiesta fiesta1 = new FeriaFiesta();
		fiesta1.setTipo("Fiesta");
		fiesta1.setCodigo("004");
		fiesta1.setNombre("San Pablo");
		fiesta1.setFechaInicio("01-04-2020");
		fiesta1.setCantidadDias(3);
		fiesta1.setMunicipio("Medell�n");
		fiesta1.setAntiguedad(3);
		
		listaFeriasyFiestas.put(fiesta1.getCodigo(), fiesta1);
	}

	private void iniciar() {
		
		// Esta linea es solopara tener datos iniciales, por defecto debe estar comentada
		llenarPruebas();
		
		// Leemos una opci�n
		String opcion = mostrarMenu();
		
		// Mientras que la opci�n no sea 10. Salir
		while (!"10".equals(opcion)) {
			switch (opcion) {
			case "1": {
				FeriaFiesta feriaFiesta = new FeriaFiesta();

				System.out.println("Digitar 1 Si va a registrar una Feria y cualquier otro numero o letra para Fiesta");
				feriaFiesta.setTipo("1".equals(escanerString.nextLine())?"Feria":"Fiesta");
				
				System.out.println("Digitar el c�digo de la " + feriaFiesta.getTipo());
				feriaFiesta.setCodigo(escanerString.nextLine());
				
				System.out.println("Digitar el nombre de la " + feriaFiesta.getTipo());
				feriaFiesta.setNombre(escanerString.nextLine());
				
				System.out.println("Digitar la Fecha de inicio de la " + feriaFiesta.getTipo());
				feriaFiesta.setFechaInicio(escanerString.nextLine());
				
				System.out.println("Digitar la duraci�n de la " + feriaFiesta.getTipo() + " en (D�as)");
				feriaFiesta.setCantidadDias(escanerEntero.nextInt());
				
				System.out.println("Digitar el Municipio de la " + feriaFiesta.getTipo());
				feriaFiesta.setMunicipio(escanerString.nextLine());
				
				System.out.println("Digitar la antig�edad de la " + feriaFiesta.getTipo() + " en (A�os)");
				feriaFiesta.setAntiguedad(escanerEntero.nextInt());
				
				// Agregamos la Feria o Fista a la lista Global
				listaFeriasyFiestas.put(feriaFiesta.getCodigo(), feriaFiesta);
				
				System.out.println("Se registr� la Feria o Fiesta correctamente");
				break;
			}
			case "2": {
				// Iteramos la lista para imprimir los valores
				for (Map.Entry<String,FeriaFiesta> feriaFiesta : listaFeriasyFiestas.entrySet())  {
					System.out.println("===========================================");
					System.out.println(feriaFiesta.getValue().getTipo().toUpperCase() + " " +feriaFiesta.getValue().getNombre().toUpperCase());
					System.out.println("C�digo: " + feriaFiesta.getValue().getNombre());
					System.out.println("Fecha de Inicio: " + feriaFiesta.getValue().getFechaInicio());
					System.out.println("Duraci�n: " + feriaFiesta.getValue().getCantidadDias() + " D�as");
					System.out.println("Municipio: " + feriaFiesta.getValue().getMunicipio());
					System.out.println("Antiguedad: " + feriaFiesta.getValue().getAntiguedad() + " A�os");
					System.out.println("===========================================");
				}
				break;
			}
			case "3" : {
				// Inicializamos los contadores necesarios para el resultado
				int contadorFerias = 0;
				int contadorFiestas = 0;
				// Iteramos la lista para saber si es feria o fiesta
				for (Map.Entry<String,FeriaFiesta> feriaFiesta : listaFeriasyFiestas.entrySet())  {
					if("Feria".equals(feriaFiesta.getValue().getTipo()))
						contadorFerias++;
					if("Fiesta".equals(feriaFiesta.getValue().getTipo()))
						contadorFiestas++;
				}
				// Imprimimos los resultados
				System.out.println("===========================================");
				System.out.println("Cantidad de Ferias: " + contadorFerias);
				System.out.println("Cantidad de Fiestas: " + contadorFiestas);
				System.out.println("===========================================");
				break;
			}
			case "4" : {
				//Solicitamos al usuario que ingrese el c�digo de la Feria o Fiesta a modificar
				System.out.println("Digitar el c�digo de la Feria o Fiesta a modificar");
				String codigoFeriaFiesta = escanerString.nextLine();
				
				// Obtenemos la Feria o Fista por la clave
				FeriaFiesta feriaFiesta = listaFeriasyFiestas.get(codigoFeriaFiesta);
				
				// Validamos que la Feria o Fiesta con dicho c�digo exista
				if(feriaFiesta!=null) {
					
					// Actualizamos los valores
					System.out.println("Digitar 1 Si es una Feria y cualquier otro numero o letra para Fiesta");
					feriaFiesta.setTipo("1".equals(escanerString.nextLine())?"Feria":"Fiesta");
					
					System.out.println("Digitar el nombre de la " + feriaFiesta.getTipo());
					feriaFiesta.setNombre(escanerString.nextLine());
					
					System.out.println("Digitar la Fecha de inicio de la " + feriaFiesta.getTipo());
					feriaFiesta.setFechaInicio(escanerString.nextLine());
					
					System.out.println("Digitar la duraci�n de la " + feriaFiesta.getTipo() + " en (D�as)");
					feriaFiesta.setCantidadDias(escanerEntero.nextInt());
					
					System.out.println("Digitar el Municipio de la " + feriaFiesta.getTipo());
					feriaFiesta.setMunicipio(escanerString.nextLine());
					
					System.out.println("Digitar la antig�edad de la " + feriaFiesta.getTipo() + " en (A�os)");
					feriaFiesta.setAntiguedad(escanerEntero.nextInt());
					
					listaFeriasyFiestas.put(feriaFiesta.getCodigo(), feriaFiesta);
					
				} else {
					// Si no existe la Feria o Fietsa mostramos un mensaje
					System.err.println("La feria o fiesta NO existe");
				}
				
				break;
			}
			case "5": {
				// Inicializamos el contador de Ferias y Fiestas de Envigado
				int contadorEnvigado = 0;
				
				for (Map.Entry<String,FeriaFiesta> feriaFiesta : listaFeriasyFiestas.entrySet())  {
					
					// Validamos si la Feria o Fietsa es de Envigado
					if("ENVIGADO".equals(feriaFiesta.getValue().getMunicipio().toUpperCase())) {
						System.out.println("===========================================");
						System.out.println(feriaFiesta.getValue().getTipo().toUpperCase() + " " +feriaFiesta.getValue().getNombre().toUpperCase());
						System.out.println("C�digo: " + feriaFiesta.getValue().getNombre());
						System.out.println("Fecha de Inicio: " + feriaFiesta.getValue().getFechaInicio());
						System.out.println("Duraci�n: " + feriaFiesta.getValue().getCantidadDias() + " D�as");
						System.out.println("Municipio: " + feriaFiesta.getValue().getMunicipio());
						System.out.println("Antiguedad: " + feriaFiesta.getValue().getAntiguedad() + " A�os");
						System.out.println("===========================================");
						contadorEnvigado++;
					}					
				}
				System.out.println("===========================================");
				// Imprimimos el total de Ferias y Fiestas en Envigado
				System.out.println("Hay en total " + contadorEnvigado + " Ferias y Fiestas en Envigado");
				System.out.println("===========================================");
				break;
			}
			case "6" : {
				// Inicializamos los contadores necesarios
				int contadorFiestas = 0;
				int sumaDuraciones = 0;
				double promedio = 0;
				
				for (Map.Entry<String,FeriaFiesta> feriaFiesta : listaFeriasyFiestas.entrySet())  {
					// Validamos si es una Fiesta para tenerla en cuenta en la operaci�n
					if("Fiesta".equals(feriaFiesta.getValue().getTipo())) {
						contadorFiestas++;
						sumaDuraciones += feriaFiesta.getValue().getCantidadDias();
					}
				}
				System.out.println("===========================================");
				//Imprimimos los valores
				System.out.println("Cantidad de Fiestas registradas: " + contadorFiestas);
				System.out.println("Suma total de duraciones: " + sumaDuraciones);
				
				// Calculamos e Imprimimos los valores
				promedio = (double)sumaDuraciones/contadorFiestas;
				System.out.println("Promedio: " + promedio);
				System.out.println("===========================================");
				break;
			}
			case "7": {
				// Inicializamos los objetos para almacenar la Feria y Fiesta mas antiguas
				FeriaFiesta feria = new FeriaFiesta();
				FeriaFiesta fiesta = new FeriaFiesta();
				for (Map.Entry<String,FeriaFiesta> feriaFiesta : listaFeriasyFiestas.entrySet())  {
					// Validamos si la Feria o Fiesta es mas antigua que la almacenada para reemplazarla
					if ("Feria".equals(feriaFiesta.getValue().getTipo()) && feria.getAntiguedad() < feriaFiesta.getValue().getAntiguedad()) {
						feria = feriaFiesta.getValue();
					} else if ("Fiesta".equals(feriaFiesta.getValue().getTipo()) && fiesta.getAntiguedad() < feriaFiesta.getValue().getAntiguedad()) {
						fiesta = feriaFiesta.getValue();
					}
				}
				System.out.println("===========================================");
				// Imprimimos los valores
				System.out.println("La Feria con mayor antig�edad es: " + feria.getNombre() + " con " + feria.getAntiguedad());
				System.out.println("La Fiesta con mayor antig�edad es: " + fiesta.getNombre() + " con " + fiesta.getAntiguedad());
				System.out.println("===========================================");
				
				break;
			}
			case "8": {
				// Inicializamos el contador de Ferias y Fiestas de Envigado
				int contadorEnvigado = 0;
				int contadorTotal = 0;
				
				for (Map.Entry<String,FeriaFiesta> feriaFiesta : listaFeriasyFiestas.entrySet())  {
					contadorTotal++;
					// Validamos si la Feria o Fietsa es de Envigado
					if("ENVIGADO".equals(feriaFiesta.getValue().getMunicipio().toUpperCase())) {
						contadorEnvigado++;
					}
				}
				
				// Sacamos el porcentaje
				float porcentaje = (float) (contadorEnvigado*100)/contadorTotal;
				
				System.out.println("===========================================");
				System.out.println("El porcentaje de Ferias y Fiestas en Envigado es de: " + porcentaje + "%");
				System.out.println("===========================================");
				
				break;
			}
			case "9": {
				
				List<FeriaFiesta> ferias = new ArrayList<>();	
				List<FeriaFiesta> fiestas = new ArrayList<>();
				
				// Llenamos una lista con solo las Ferias
				for (Map.Entry<String,FeriaFiesta> feriaFiesta : listaFeriasyFiestas.entrySet())  {
					if ("Feria".equals(feriaFiesta.getValue().getTipo())) {
						ferias.add(feriaFiesta.getValue());
					}
				}
				
				// Llenamos una lista con solo las Fiestas
				for (Map.Entry<String,FeriaFiesta> feriaFiesta : listaFeriasyFiestas.entrySet())  {
					if ("Fiesta".equals(feriaFiesta.getValue().getTipo())) {
						fiestas.add(feriaFiesta.getValue());
					}
				}
				
				// Ordenamos
				ferias.sort(Comparator.comparing(FeriaFiesta::getAntiguedad));
				fiestas.sort(Comparator.comparing(FeriaFiesta::getAntiguedad));
				
				// Imprimimos las Ferias Ordenadas
				System.out.println("===========================================");
				System.out.println("Ferias ordenadas acendentemente por A�os de antiguedad:");
				for (FeriaFiesta feria : ferias) {
					System.out.println("###########################################");
					System.out.println(feria.getTipo().toUpperCase() + " " +feria.getNombre().toUpperCase());
					System.out.println("C�digo: " + feria.getNombre());
					System.out.println("Fecha de Inicio: " + feria.getFechaInicio());
					System.out.println("Duraci�n: " + feria.getCantidadDias() + " D�as");
					System.out.println("Municipio: " + feria.getMunicipio());
					System.out.println("Antiguedad: " + feria.getAntiguedad() + " A�os");
				}
				System.out.println("===========================================");
				
				// Imprimimos las Fiestas Ordenadas
				System.out.println("===========================================");
				System.out.println("Fiestas ordenadas acendentemente por A�os de antiguedad:");
				for (FeriaFiesta fiesta : fiestas) {
					System.out.println("###########################################");
					System.out.println(fiesta.getTipo().toUpperCase() + " " +fiesta.getNombre().toUpperCase());
					System.out.println("C�digo: " + fiesta.getNombre());
					System.out.println("Fecha de Inicio: " + fiesta.getFechaInicio());
					System.out.println("Duraci�n: " + fiesta.getCantidadDias() + " D�as");
					System.out.println("Municipio: " + fiesta.getMunicipio());
					System.out.println("Antiguedad: " + fiesta.getAntiguedad() + " A�os");
				}
				System.out.println("===========================================");
				
				break;
			}
			default:
				//Se deja una opci�n por default si la persona digita una opci�n incorrecta
				System.out.println("La opci�n no es valida");
			}
			
			opcion = mostrarMenu();
		}
		//Mostramos un mensaje de que el sistema termin� correctamente
		System.out.println("Se sali� del sistema correctamente");
		
	}

	/**
	 * Metodo para imprimir el men�
	 * 
	 * @return la opci�n digitada por el usuario
	 */
	private String mostrarMenu() {
		System.out.println("Que desea realizar?");
		System.out.println("1. Registrar una feria o fiesta");
		System.out.println("2. Mostrar la informaci�n de todas las Ferias y Fiestas registradas");
		System.out.println("3. Mostrar la cantidad de Ferias o Fiestas registradas");
		System.out.println("4. Modificar la informaci�n de una Feria o Fiesta");
		System.out.println("5. Mostrar las Ferias y Fiestas de Envigado");
		System.out.println("6. Promedio de las duraciones de fiestas");
		System.out.println("7. Mostrar el nombre de la Feria y Fiesta con mayor antiguedad");
		System.out.println("8. Porcentaje de Ferias y Fiestas en Envigado");
		System.out.println("9. Mostrar informaci�n de ferias y fiestas en orden ascendente por antig�edad");
		System.out.println("10. Para salir. 'Se perder�n todos los datos guardados'");
		return escanerString.nextLine();
	}
	
	

}
